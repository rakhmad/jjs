// (C) 2013 Kim, Taegyoon
// jjs: JavaScript in Java

public class jjs {
	public static void main(String[] args) {		
		if (args.length == 0) {
			libjjs js = new libjjs();
            libjjs.print_logo();
            js.repl();
            System.out.println(js.engine.get("a")); // retrieve a value            
            System.out.println();
            return;
        } else if (args.length == 1) {
            if (args[0].equals("-h")) {
                System.out.println("Usage: java jjs [OPTIONS...] [FILES...]");
                System.out.println();
                System.out.println("OPTIONS:");
                System.out.println("    -h    print this screen.");
                System.out.println("    -v    print version.");
                return;
            } else if (args[0].equals("-v")) {
                System.out.println(libjjs.VERSION);
                return;
            }
        }        
        
        // execute files, one by one
        for (String fileName : args) {
        	libjjs js = new libjjs();
            try {
                js.eval(new java.io.FileReader(fileName));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
	}
}
