// (C) 2013 Kim, Taegyoon
// jjs: JavaScript in Java

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.script.ScriptException;

public class libjjs {
	public static final String VERSION = "0.1.1";
	
	public javax.script.ScriptEngineManager factory = new javax.script.ScriptEngineManager(); // create a script engine manager
	public javax.script.ScriptEngine engine = factory.getEngineByName("JavaScript"); // create a JavaScript engine

	public Object eval(String s) throws ScriptException {
		return engine.eval(s);
	}
	
	public Object eval(Reader r) throws ScriptException {
		return engine.eval(r);
	}
	
    public static void print_logo() {
        System.out.println(
            "jjs (JavaScript in Java) " + VERSION + " (C) 2013 Kim, Taegyoon\n" +
            "Press Enter key twice to evaluate.");
    }
    
    private static void prompt() {
        System.out.print("> ");
    }
    
    private static void prompt2() {
        System.out.print("  ");
    }  
    
    // read-eval-print loop
    public void repl() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String code = "";
        while (true) {
            try {
                if (code.length() == 0) prompt(); else prompt2();
                String line = br.readLine();
                if (line == null) { // EOF
                	Object ret = eval(code);
                	String type = "";
                	if (ret != null) type = " : " + ret.getClass().getName();
                    System.out.println(ret + type);
                    break;
                }
                code += "\n" + line;
                if (line.length() == 0) {
                	Object ret = eval(code);
                	String type = "";
                	if (ret != null) type = " : " + ret.getClass().getName();
                    System.out.println(ret + type);
                    code = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
