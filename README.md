# jjs: JavaScript in Java

(C) 2013 Kim, Taegyoon

jjs is JavaScript in Java. 
It is designed to be an easy-to-use library and REPL. You can use Java in your jjs program.

## Requirement
Java 1.6 or higher

## Compile ##
```
cd src
javac libjjs.java jjs.java
```

## Run ##
```
Usage: java jjs [OPTIONS...] [FILES...]

OPTIONS:
    -h    print this screen.
    -v    print version.

```

## Files ##
* libjjs.java: jjs language library
* jjs.java: jjs REPL executable

## JavaScript Reference ##
* [Java Scripting Programmer's Guide](http://docs.oracle.com/javase/7/docs/technotes/guides/scripting/programmer_guide/index.html)
* [https://developer.mozilla.org/en-US/docs/JavaScript/Reference](https://developer.mozilla.org/en-US/docs/JavaScript/Reference)

## Examples ##
### Hello, World! ###
```
> println("Hello, World!");
  
Hello, World!
null
```

### Function ###

```
> (function(x,y) {return x+y;})(1,2)
  
3.0 : java.lang.Double
> function sum(x,y) {return x+y;}
  sum(1,2)  
  
3.0 : java.lang.Double
> "abc"=="abc"
  
true : java.lang.Boolean
```

### Array ###
```
> [2,4,6][1]
  
4.0 : java.lang.Double
> [1,2,3].length
  
3.0 : java.lang.Double
```

### Java interoperability (from jjs) ###
```
> java.lang.Math.random()
  
0.6199166488607881 : java.lang.Double
> java.lang.Math.floor(1.5)
  
1.0 : java.lang.Double
> (3).toString()
  
3 : java.lang.String
> java.lang.Math.PI
  
3.141592653589793 : java.lang.Double
> new java.math.BigInteger("2").pow(100) // 2 ^ 100
  
1267650600228229401496703205376 : java.math.BigInteger
```

### Java interoperability (from Java) ###
```
try {
	js.eval("a=3;");
} catch (javax.script.ScriptException e) {
	e.printStackTrace();
}
System.out.println(js.engine.get("a")); // retrieve a value
```

### [Project Euler Problem 1](http://projecteuler.net/problem=1) ###
```
var sum = 0;
for (var i = 1; i < 1000; i++) {
  if (i % 3 == 0 || i % 5 == 0) {
    sum += i;
  }
}
println(sum);
```
=> 233168

### [Project Euler Problem 2](http://projecteuler.net/problem=2) ###
```
var a=1, b=1, sum=0; while(a <= 4000000) { if (a % 2 == 0) sum += a; var c=a+b; a=b; b=c; } println(sum);
```
=> 4613732

### [Project Euler Problem 3](http://projecteuler.net/problem=3) ###
```
var num = 600851475143;
var p;
for (p = 2; ; p++) {
 while (num%p == 0)
  num /= p;
 if (num <= 1)
  break;
}
println(p);
```
=> 6857

### [Project Euler Problem 4](http://projecteuler.net/problem=4) ###
```
var isPalindrome = function(value) {
    var to = value.length / 2;
    for (var i = 0; i < to; i++) {
        if (value[i] != value[value.length-1-i]) {
            return false;
        }
    }
    return true;
}
var problem4 = function() {
    var maxP = 0;
    for (var i = 100; i <= 999; i++) {
        for (var j = 100; j <= 999; j++) {
            var p = i * j;
            if (isPalindrome(p.toString())) {
                if (p > maxP) {
                    maxP = p;
                }
            }
        }
    }
    return maxP;
}
println(problem4());
```
=> 906609

## License ##

   Copyright 2013 Kim, Taegyoon

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
